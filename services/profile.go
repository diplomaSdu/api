package services

import (
	"diplomka-api/diplomkaDb"
	"diplomka-api/structures"
	"errors"
	"math/rand"
	"golang.org/x/crypto/bcrypt"
	"strings"
	"time"
	"unicode"
)

var letterRunes = []rune("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ")

func GenerateRandomString(n int) string {
	b := make([]rune, n)
	for i := range b {
		b[i] = letterRunes[rand.Intn(len(letterRunes))]
	}
	return string(b)
}

func RegisterStudent(student structures.Student) (bool, error) {
	authKey := GenerateRandomString(32)
	passwordHash := GetHash(student.Password)
	return diplomkaDb.InsertIntoStudents(student.Email, passwordHash, authKey)
}

func RegisterCompany(company structures.Company) (bool, error) {
	authKey := GenerateRandomString(32)
	passwordHash := GetHash(company.Password)
	createdAt := time.Now().Format("2006-01-02 15:04:05")
	return diplomkaDb.InsertIntoCompanies(company.Name, company.Email, company.Bin, passwordHash, authKey, createdAt)
}

func GetHash(str string) string {
	bytePassword := []byte(str)
	passwordHash, _ := bcrypt.GenerateFromPassword(bytePassword, bcrypt.DefaultCost)
	return string(passwordHash)
}

func LoginStudent(loged_student structures.Student) (structures.Student, error)  {
	student, err := diplomkaDb.GetStudentByEmail(loged_student.Email)
	if err != nil {
		if strings.Contains(err.Error(), "no rows in result set") {
			return structures.Student{}, errors.New("этот email не зарегистрирован")
		}
		panic(err)
	}
	err = bcrypt.CompareHashAndPassword([]byte(student.PasswordHash), []byte(loged_student.Password))
	println(student.PasswordHash, loged_student.Password)
	if err == nil {
		if student.CityId != nil {
			city, cErr := diplomkaDb.GetCity(*student.CityId)
			if cErr == nil {
				student.City = city
			}
		}
		if student.UniversityId != nil {
			university, uErr := diplomkaDb.GetUniversity(*student.UniversityId)
			if uErr == nil {
				if university.CityId != nil {
					uCity, ucErr := diplomkaDb.GetCity(*university.CityId)
					if ucErr == nil {
						university.City = uCity
					}
				}
				student.University = university
			}
		}
		if student.SpecialityId != nil {
			speciality, sErr := diplomkaDb.GetSpeciality(*student.SpecialityId)
			if sErr == nil {
				student.Speciality = speciality
			}
		}

		return student, err
	}

	return structures.Student{}, errors.New("не верный логин/пароль")
}

func LoginCompany(loged_company structures.Company) (structures.Company, error)  {
	company, err := diplomkaDb.GetCompanyByEmail(loged_company.Email)
	if err != nil {
		if strings.Contains(err.Error(), "no rows in result set") {
			return structures.Company{}, errors.New("этот email не зарегистрирован")
		}
		panic(err)
	}
	err = bcrypt.CompareHashAndPassword([]byte(company.PasswordHash), []byte(loged_company.Password))
	println(company.PasswordHash, loged_company.Password)
	if err == nil {
		return company, err
	}

	return structures.Company{}, errors.New("не верный логин/пароль")
}

func UpdateStudentProfile(student structures.StudentApi) (bool, error) {
	success, err := diplomkaDb.UpdateStudent(student)
	return success, err
}

func UpdateCompanyProfile(company structures.CompanyApi) (bool, error) {
	updatedAt := time.Now().Format("2006-01-02 15:04:05")
	company.UpdatedAt = updatedAt
	success, err := diplomkaDb.UpdateCompany(company)
	return success, err
}

func ValidateRegistration(email string, password string) (bool, structures.Response) {
	var response structures.Response
	if password == "" || email == "" {
		response.Error = "Отсутвствуют логин/пароль"
		response.Success = false

		return false, response
	}
	if len(password) < 9 || !unicode.IsUpper(rune(password[0])) {
		response.Error = "Пароль должен содержать не менее 8 символов и начинаться с заглавной буквы"
		response.Success = false

		return false, response
	}
	return true, response
}