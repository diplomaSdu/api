package services

import (
	"diplomka-api/diplomkaDb"
	"diplomka-api/structures"
	"time"
)

func AddVisitedStudent(visit structures.VisitStudentApi) (bool, error) {
	visit.VisitTime = time.Now().Format("2006-01-02 15:04:05")
	return diplomkaDb.InsertStudentVisit(visit)
}

func AddVisitedOffer(visit structures.VisitOfferApi) (bool, error) {
	visit.VisitTime = time.Now().Format("2006-01-02 15:04:05")
	return diplomkaDb.InsertOfferVisit(visit)
}