package structures

type Student struct {
	Id            int        `json:"id"`
	FullName      *string    `json:"full_name"`
	PasswordHash  string     `json:"password_hash"`
	Password      string     `json:"password"`
	BirthDate     *string    `json:"birth_date"`
	Gender        *int       `json:"gender"`
	UniversityId  *int       `json:"university_id"`
	University    University `json:"university"`
	SpecialityId  *int       `json:"speciality_id"`
	Speciality    Speciality `json:"speciality"`
	AdmissionDate *string    `json:"admission_date"`
	DegreeId      *int       `json:"degree_id"`
	CityId        *int       `json:"city_id"`
	City          City       `json:"city"`
	AbleToMove    bool       `json:"able_to_move"`
	Email         string     `json:"email"`
	Telegram      *string    `json:"telegram"`
	Phone         *string    `json:"phone"`
	IsActive      bool       `json:"is_active"`
	AuthKey       string     `json:"auth_key"`
	CreatedAt     int        `json:"createdAt"`
}

type University struct {
	Id     int     `json:"id"`
	Name   string  `json:"name"`
	Rating float32 `json:"rating"`
	CityId *int    `json:"city_id"`
	City   City    `json:"city"`
	Review *string `json:"review"`
}

type City struct {
	Id   int    `json:"id"`
	Name string `json:"name"`
}

type Speciality struct {
	Id   int    `json:"id"`
	Name string `json:"name"`
}

type StudentApi struct {
	Id            int    `json:"id"`
	FullName      string `json:"fullName"`
	PasswordHash  string `json:"passwordHash"`
	Password      string `json:"password"`
	BirthDate     string `json:"birthDate"`
	Gender        int    `json:"gender"`
	UniversityId  int    `json:"universityId"`
	SpecialityId  int    `json:"specialityId"`
	AdmissionDate string `json:"admissionDate"`
	DegreeId      int    `json:"degreeId"`
	CityId        int    `json:"cityId"`
	AbleToMove    bool   `json:"ableToMove"`
	Email         string `json:"email"`
	Telegram      string `json:"telegram"`
	Phone         string `json:"phone"`
	IsActive      bool   `json:"isActive"`
	AuthKey       string `json:"authKey"`
	CreatedAt     int    `json:"createdAt"`
}

type Company struct {
	Id           int     `json:"id"`
	Name         string  `json:"name"`
	Image        *string `json:"image"`
	Email        string  `json:"email"`
	Description  *string `json:"description"`
	Rating       float32 `json:"rating"`
	CityId       *int    `json:"city_id"`
	Bin          string  `json:"bin"`
	IsActive     bool    `json:"is_active"`
	CreatedAt    string  `json:"created_at"`
	UpdatedAt    *string `json:"updated_at"`
	PasswordHash string  `json:"password_hash"`
	Password     string  `json:"password"`
	AuthKey      string  `json:"auth_key"`
}

type CompanyApi struct {
	Id           int     `json:"id"`
	Name         string  `json:"name"`
	Image        string  `json:"image"`
	Email        string  `json:"email"`
	Description  string  `json:"description"`
	Rating       float32 `json:"rating"`
	CityId       int     `json:"city_id"`
	Bin          string  `json:"bin"`
	IsActive     bool    `json:"is_active"`
	CreatedAt    string  `json:"created_at"`
	UpdatedAt    string  `json:"updated_at"`
	PasswordHash string  `json:"password_hash"`
	Password     string  `json:"password"`
	AuthKey      string  `json:"auth_key"`
}
