package structures

type Response struct {
	Success bool   `json:"success"`
	Error   string `json:"error"`
}

type StudentLoginResponse struct {
	Success bool    `json:"success"`
	Error   string  `json:"error"`
	Student Student `json:"data"`
}

type CompanyLoginResponse struct {
	Success bool    `json:"success"`
	Error   string  `json:"error"`
	Company Company `json:"data"`
}
