package structures

type VisitStudent struct {
	VisitedCompanyName string `json:"visited_company_name"`
	StudentId          int    `json:"student_id"`
	VisitTime          string `json:"visit_time"`
}

type VisitStudentApi struct {
	VisitedCompanyName string `json:"visitedCompanyName"`
	StudentId          int    `json:"studentId"`
	VisitTime          string `json:"visitTime"`
}

type VisitOffer struct {
	OfferId   string `json:"offer_id"`
	StudentId int    `json:"student_id"`
	VisitTime string `json:"visit_time"`
}

type VisitOfferApi struct {
	OfferId   int    `json:"offerId"`
	StudentId int    `json:"studentId"`
	VisitTime string `json:"visitTime"`
}
