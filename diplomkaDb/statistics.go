package diplomkaDb

import (
	"diplomka-api/structures"
	"fmt"
)

func InsertStudentVisit(visit structures.VisitStudentApi) (bool, error) {
	query := fmt.Sprintf(
		`INSERT INTO ch.student_visited_stat(visited_company_name, student_id, visit_time) VALUES ('%s', %d, '%s')`,
		visit.VisitedCompanyName, visit.StudentId, visit.VisitTime)
	println(query)
	err := chDb.Exec(query)
	if err != nil {
		return false, err
	}
	return true, nil
}

func InsertOfferVisit(visit structures.VisitOfferApi) (bool, error) {
	query := fmt.Sprintf(
		`INSERT INTO ch.offer_visited_stat(offer_id, student_id, visit_time) VALUES (%d, %d, '%s')`,
		visit.OfferId, visit.StudentId, visit.VisitTime)
	println(query)
	err := chDb.Exec(query)
	if err != nil {
		return false, err
	}
	return true, nil
}