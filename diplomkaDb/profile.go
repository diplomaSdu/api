package diplomkaDb

import (
	"diplomka-api/structures"
	"fmt"
	"reflect"
	"strconv"
	"strings"
)

func InsertIntoStudents(email string, password string, authKey string) (bool, error) {
	_, err := diplomkaDb.Query(`INSERT INTO students(full_name, email, password_hash, auth_key) VALUES ('', '` + email + `','` +
		password + `','` + authKey + `');`)

	if err != nil {
		return false, err
	}
	return true, nil
}

func InsertIntoCompanies(name string, email string, bin string, password string, authKey string,  createdAt string) (bool, error) {
	_, err := diplomkaDb.Query(`INSERT INTO companies(name, email, bin, password_hash, auth_key, created_at, updated_at) VALUES ('` + name +
		`','` + email + `','` + bin + `','` + password + `','` + authKey + `','`+ createdAt +`','`+ createdAt +`');`)

	if err != nil {
		return false, err
	}
	return true, nil
}

func GetStudentByEmail(email string) (structures.Student, error) {
	query := diplomkaDb.QueryRow(`SELECT id, full_name, email, birth_date, auth_key, gender, able_to_move,
			admission_date, city_id, degree_id, is_active, phone, telegram, 
			speciality_id, university_id, password_hash FROM students WHERE email LIKE '` + email + `'`)
	var student = structures.Student{}

	err := query.Scan(&student.Id, &student.FullName, &student.Email, &student.BirthDate, &student.AuthKey,
		&student.Gender, &student.AbleToMove, &student.AdmissionDate, &student.CityId, &student.DegreeId,
		&student.IsActive, &student.Phone, &student.Telegram, &student.SpecialityId,
		&student.UniversityId, &student.PasswordHash)

	return student, err
}

func GetUniversity(id int) (structures.University, error) {
	query := diplomkaDb.QueryRow(`SELECT id, name, rating, city_id, review FROM universities WHERE id = ` + strconv.Itoa(id))
	var university = structures.University{}

	err := query.Scan(&university.Id, &university.Name, &university.Rating, &university.CityId, &university.Review)

	return university, err
}

func GetCity(id int) (structures.City, error) {
	query := diplomkaDb.QueryRow(`SELECT id, name FROM cities WHERE id = ` + strconv.Itoa(id))
	var city = structures.City{}

	err := query.Scan(&city.Id, &city.Name)

	return city, err
}

func GetSpeciality(id int) (structures.Speciality, error) {
	query := diplomkaDb.QueryRow(`SELECT id, name FROM specialities WHERE id = ` + strconv.Itoa(id))
	var speciality = structures.Speciality{}

	err := query.Scan(&speciality.Id, &speciality.Name)

	return speciality, err
}

func GetCompanyByEmail(email string) (structures.Company, error) {
	query := diplomkaDb.QueryRow(`SELECT id, name, description, rating, city_id, bin, is_active,
			created_at, updated_at, auth_key, email, image FROM companies WHERE email LIKE '` + email + `'`)
	var company = structures.Company{}

	err := query.Scan(&company.Id, &company.Name, &company.Description, &company.Rating, &company.CityId, &company.Bin,
		&company.IsActive, &company.CreatedAt, &company.UpdatedAt, &company.AuthKey, &company.Email, &company.Image)

	return company, err
}

func GetStudentByAuthToken(authToken string) (structures.Student, error) {
	query := diplomkaDb.QueryRow(`SELECT id, full_name, email, birth_date, auth_key, gender, able_to_move,
			admission_date, city_id, degree_id, is_active, phone, telegram, 
			speciality_id, university_id, password_hash FROM students WHERE auth_key LIKE '` + authToken + `'`)
	var student = structures.Student{}

	err := query.Scan(&student.Id, &student.FullName, &student.Email, &student.BirthDate, &student.AuthKey,
		&student.Gender, &student.AbleToMove, &student.AdmissionDate, &student.City.Id, &student.DegreeId,
		&student.IsActive, &student.Phone, &student.Telegram, &student.Speciality.Id, &student.University.Id, &student.PasswordHash)
	if err != nil {
		if strings.Contains(err.Error(), "no rows in result set") {
			return structures.Student{}, err
		}
		panic(err)
	}

	return student, err
}

func GetCompanyByAuthToken(authToken string) (structures.Company, error) {
	query := diplomkaDb.QueryRow(`SELECT id, name, description, rating, city_id, bin, is_active,
			created_at, updated_at, auth_key, password_hash, email, image FROM companies WHERE auth_key LIKE '` + authToken + `'`)
	var company = structures.Company{}

	err := query.Scan(&company.Id, &company.Name, &company.Description, &company.Rating, &company.CityId, &company.Bin,
		&company.IsActive, &company.CreatedAt, &company.UpdatedAt, &company.AuthKey, &company.PasswordHash, &company.Email,
		&company.Image)
	if err != nil {
		if strings.Contains(err.Error(), "no rows in result set") {
			return structures.Company{}, err
		}
		panic(err)
	}

	return company, err
}

func UpdateCompany(company structures.CompanyApi) (bool, error) {
	cityId := strconv.Itoa(company.CityId)
	rating := fmt.Sprintf("%f", company.Rating)

	if cityId == "0" {
		cityId = "NULL"
	}
	if rating == "0" {
		rating = "NULL"
	}

	query := fmt.Sprintf(
		"UPDATE companies SET name = '%s', description = '%s', rating = %s, city_id = %s, bin = %s," +
			" is_active = '%s', updated_at = %s, email = %s, image = %s WHERE id = %d",
		company.Name, company.Description, rating, cityId, company.Bin, strconv.FormatBool(company.IsActive), company.UpdatedAt,
		company.Email, company.Image, company.Id)

	println(query)

	_, err := diplomkaDb.Query(query)

	if err != nil {
		return false, err
	}
	return true, nil
}

func UpdateStudent(student structures.StudentApi) (bool, error) {
	gender := strconv.Itoa(student.Gender)
	universityId := strconv.Itoa(student.UniversityId)
	specialityId := strconv.Itoa(student.SpecialityId)
	cityId := strconv.Itoa(student.CityId)

	if universityId == "0" {
		universityId = "NULL"
	}
	if specialityId == "0" {
		specialityId = "NULL"
	}
	if cityId == "0" {
		cityId = "NULL"
	}
	if gender == "0" {
		gender = "NULL"
	}

	query := fmt.Sprintf(
		"UPDATE students SET full_name = '%s', birth_date = '%s', gender = %s, university_id = %s, speciality_id = %s," +
			" admission_date = '%s', degree_id = %s, city_id = %s, able_to_move = %s, email = '%s', telegram = '%s'," +
			" phone = '%s', is_active = '%s' WHERE id = %d",
		student.FullName, student.BirthDate, gender, universityId, specialityId,
		student.AdmissionDate, strconv.Itoa(student.DegreeId), cityId, strconv.FormatBool(student.AbleToMove), student.Email, student.Telegram,
		student.Phone, strconv.FormatBool(student.IsActive), student.Id)

	println(query)

	_, err := diplomkaDb.Query(query)

	if err != nil {
		return false, err
	}
	return true, nil
}

func SetResetToken(email string, token string, resetTable string)  {
	_, err := diplomkaDb.Exec(`UPDATE `+resetTable+` SET password_reset_token = '`+token+`' WHERE email LIKE '` + email + `'`)
	if err != nil {
		panic(err)
	}
}

func convertNulls(data interface{})  {
	var arrKeys []reflect.Value
	v := reflect.ValueOf(data)
	println("here")
	println(reflect.Map)
	if v.Kind() == reflect.Map {
		for i, key := range v.MapKeys() {
			arrKeys[i] = v.MapIndex(key)
			println(arrKeys[i].String())
		}
	}
}
