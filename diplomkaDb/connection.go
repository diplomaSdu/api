package diplomkaDb

import (
	"database/sql"
	_ "github.com/go-sql-driver/mysql"
	ch "github.com/leprosus/golang-clickhouse"
	_ "github.com/lib/pq"
	"log"
	"strconv"
)

var diplomkaDb *sql.DB
var chDb *ch.Conn

func InitDB(driver string, connection string) {
	log.Println(connection)
	var err error
	diplomkaDb, err = sql.Open(driver, connection)
	if err != nil {
		panic(err)
	}

	err = diplomkaDb.Ping()
	if err != nil {
		panic(err)
	}
}

func InitChDb(host string, port int) {
	chDb = ch.New(host, port, "default", "")
}

func GetDbStats() {
	stats := diplomkaDb.Stats()
	log.Println("MaxOpenConnections: " + strconv.Itoa(stats.MaxOpenConnections))
	log.Println("OpenConnections: " + strconv.Itoa(stats.OpenConnections))
	log.Println("In Use: " + strconv.Itoa(stats.InUse))
	log.Println("Idle: " + strconv.Itoa(stats.Idle))
	log.Println("WaitCount: " + strconv.Itoa(int(stats.WaitCount)))
	log.Println("WaitDuration: " + stats.WaitDuration.String())
	log.Println("MaxIdleClosed: " + strconv.Itoa(int(stats.MaxIdleClosed)))
	log.Println("MaxLifetimeClosed: " + strconv.Itoa(int(stats.MaxLifetimeClosed)))
}
