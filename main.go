package main

import (
	"diplomka-api/diplomkaDb"
	"diplomka-api/services"
	"diplomka-api/structures"
	"encoding/json"
	"github.com/gorilla/mux"
	"github.com/subosito/gotenv"
	"log"
	"net/http"
	"os"
	"strconv"
	"strings"
)

func init()  {
	gotenv.Load()
}

func main()  {
	dbDriver := "postgres"
	dbUser := os.Getenv("DB_USERNAME")
	dbPass := os.Getenv("DB_PASSWORD")
	dbName := os.Getenv("DB_NAME")
	dbHost := os.Getenv("DB_HOST")
	dbPort := os.Getenv("DB_PORT")

	chHost := os.Getenv("CH_HOST")
	chPort, _ := strconv.Atoi(os.Getenv("CH_PORT"))

	diplomkaDb.InitDB(dbDriver, "host="+dbHost+" port="+dbPort+" user="+dbUser+" password="+dbPass+" dbname="+dbName+" sslmode=disable")
	diplomkaDb.InitChDb(chHost, chPort)

	router := mux.NewRouter()
	router.HandleFunc("/registration/{type}", registration).Methods("POST")
	router.HandleFunc("/registration/{type}", options).Methods("OPTIONS")
	router.HandleFunc("/login/{type}", login).Methods("POST")
	router.HandleFunc("/login/{type}", options).Methods("OPTIONS")
	router.HandleFunc("/edit-profile/{type}", profileEdit).Methods("POST")
	router.HandleFunc("/edit-profile/{type}", options).Methods("OPTIONS")
	router.HandleFunc("/visit/{type}", addVisitRecord).Methods("POST")
	router.HandleFunc("/visit/{type}", options).Methods("OPTIONS")
	router.HandleFunc("/password-recovery/{type}", passwordRecovery).Methods("POST")
	router.HandleFunc("/password-recovery/{type}", options).Methods("OPTIONS")

	log.Println("Server started")
	log.Fatal(http.ListenAndServe(":8008", router))
}

func login(w http.ResponseWriter, r *http.Request)  {
	setHeaders(w, r)
	params := mux.Vars(r)
	loginType := params["type"]
	decoder := json.NewDecoder(r.Body)
	if loginType == "student" {
		var student structures.Student
		var response structures.StudentLoginResponse

		err := decoder.Decode(&student)
		if err != nil {
			panic(err)
		}

		authorizedStudent, err := services.LoginStudent(student)
		response.Student = authorizedStudent
		response.Success = true

		if err != nil {
			response.Error = err.Error()
			response.Success = false
		}

		w.WriteHeader(http.StatusOK)
		json.NewEncoder(w).Encode(response)
	}
	if loginType == "company" {
		var company structures.Company
		var response structures.CompanyLoginResponse

		err := decoder.Decode(&company)
		if err != nil {
			panic(err)
		}

		authorizedCompany, err := services.LoginCompany(company)
		response.Company = authorizedCompany
		response.Success = true
		if err != nil {
			response.Error = err.Error()
			response.Success = false
		}
		w.WriteHeader(http.StatusOK)
		json.NewEncoder(w).Encode(response)
	}
}

func passwordRecovery(w http.ResponseWriter, r *http.Request)  {
	setHeaders(w, r)
	params := mux.Vars(r)
	visitType := params["type"]
	decoder := json.NewDecoder(r.Body)
	var response structures.Response

	if visitType == "student" {
		var student structures.Student
		err := decoder.Decode(&student)
		if err != nil {
			panic(err)
		}
		existsStudent, err := diplomkaDb.GetStudentByEmail(student.Email)
		if err != nil {
			response.Error = err.Error()
			response.Success = false
			w.WriteHeader(http.StatusOK)
			if  strings.Contains(err.Error(), "no rows in result set") {
				response.Error = "Пользователь с такой почтой не зарегистрирован!"
			}
			json.NewEncoder(w).Encode(response)
			return
		} else {
			services.SendRecoveryPasswordEmail(existsStudent.Email, "students")
		}
	}
	if visitType == "company" {
		var company structures.Company
		err := decoder.Decode(&company)
		if err != nil {
			panic(err)
		}
		existsCompany, err := diplomkaDb.GetCompanyByEmail(company.Email)
		if err != nil {
			response.Error = err.Error()
			response.Success = false
			w.WriteHeader(http.StatusOK)
			if  strings.Contains(err.Error(), "no rows in result set") {
				response.Error = "Пользователь с такой почтой не зарегистрирован!"
			}
			json.NewEncoder(w).Encode(response)
			return
		} else {
			services.SendRecoveryPasswordEmail(existsCompany.Email, "companies")
		}
	}
	response.Success = true
	w.WriteHeader(http.StatusOK)
	json.NewEncoder(w).Encode(response)
}

func addVisitRecord(w http.ResponseWriter, r *http.Request)  {
	setHeaders(w, r)
	params := mux.Vars(r)
	visitType := params["type"]
	decoder := json.NewDecoder(r.Body)
	var response structures.Response

	if visitType != "student" && visitType != "offer" {
		response.Success = false
		response.Error = "Не верный тип! Виды типов посещения: student, offer"
	}
	if visitType == "student" {
		authSuccess, authCompany := checkAuthCompany(w, r)
		if !authSuccess {
			return
		}
		var studentVisit structures.VisitStudentApi
		err := decoder.Decode(&studentVisit)
		if err != nil {
			panic(err)
		}
		studentVisit.VisitedCompanyName = authCompany.Name
		success, err := services.AddVisitedStudent(studentVisit)
		response.Success = success
		if err != nil {
			response.Error = err.Error()
		}
	}
	if visitType == "offer" {
		authSuccess, authStudent := checkAuthStudent(w, r)
		if !authSuccess {
			return
		}
		var offerVisit structures.VisitOfferApi
		err := decoder.Decode(&offerVisit)
		offerVisit.StudentId = authStudent.Id
		if err != nil {
			panic(err)
		}
		success, err := services.AddVisitedOffer(offerVisit)
		response.Success = success
		if err != nil {
			response.Error = err.Error()
		}
	}
	w.WriteHeader(http.StatusOK)
	json.NewEncoder(w).Encode(response)
}

func registration(w http.ResponseWriter, r *http.Request)  {
	setHeaders(w, r)
	params := mux.Vars(r)
	registrationType := params["type"]
	decoder := json.NewDecoder(r.Body)
	var response structures.Response

	if registrationType == "student" {
		var student structures.Student
		err := decoder.Decode(&student)
		validate, errResponse := services.ValidateRegistration(student.Email, student.Password)
		if !validate {
			w.WriteHeader(http.StatusOK)
			json.NewEncoder(w).Encode(errResponse)
			return
		}
		if err != nil {
			panic(err)
		}

		success, err := services.RegisterStudent(student)
		response.Success = success
		if err != nil {
			if strings.Contains(err.Error(), "violates unique constraint") {
				response.Error = "Email уже занят!"
			} else {
				response.Error = err.Error()
			}
		}

		w.WriteHeader(http.StatusOK)
		json.NewEncoder(w).Encode(response)
	} else if registrationType == "company" {
		var company structures.Company

		err := decoder.Decode(&company)
		if err != nil {
			panic(err)
		}

		success, err := services.RegisterCompany(company)
		response.Success = success
		if err != nil {
			if strings.Contains(err.Error(), "violates unique constraint") {
				response.Error = "Бин уже занят!"
			} else {
				response.Error = err.Error()
			}
		}

		w.WriteHeader(http.StatusOK)
		json.NewEncoder(w).Encode(response)
	}
}

func profileEdit(w http.ResponseWriter, r *http.Request) {
	setHeaders(w, r)
	params := mux.Vars(r)
	reqType := params["type"]

	var response structures.Response
	decoder := json.NewDecoder(r.Body)
	if reqType == "student" {
		success, authStudent := checkAuthStudent(w ,r)
		if !success {
			return
		}
		var student structures.StudentApi
		err := decoder.Decode(&student)

		if err != nil {
			panic(err)
		}
		student.Id = authStudent.Id
		res, err := services.UpdateStudentProfile(student)
		response.Success = res
		if err != nil {
			response.Error = err.Error()
		}
	}
	if reqType == "company" {
		success, authCompany := checkAuthCompany(w, r)
		if !success {
			return
		}
		var company structures.CompanyApi
		err := decoder.Decode(&company)
		if err != nil {
			panic(err)
		}
		company.Id = authCompany.Id
		res, err := services.UpdateCompanyProfile(company)
		response.Success = res
		if err != nil {
			response.Error = err.Error()
		}
	}
	w.WriteHeader(http.StatusOK)
	json.NewEncoder(w).Encode(response)
}

func checkAuthStudent(w http.ResponseWriter, r *http.Request) (bool, structures.Student) {
	authToken := r.Header.Get("x-auth-token")

	student, err := diplomkaDb.GetStudentByAuthToken(authToken)
	var response structures.Response

	if  authToken == "" || err != nil || student.Email == "" {
		response.Success = false
		response.Error = "Не авторизованный запрос"
		w.WriteHeader(http.StatusUnauthorized)
		json.NewEncoder(w).Encode(response)
		return false, student
	}
	return true, student
}

func checkAuthCompany(w http.ResponseWriter, r *http.Request) (bool, structures.Company) {
	authToken := r.Header.Get("x-auth-token")

	company, err := diplomkaDb.GetCompanyByAuthToken(authToken)
	var response structures.Response

	if  authToken == "" || err != nil || company.Email == "" {
		response.Success = false
		response.Error = "Не авторизованный запрос"
		w.WriteHeader(http.StatusUnauthorized)
		json.NewEncoder(w).Encode(response)
		return false, company
	}
	return true, company
}

func options(w http.ResponseWriter, r *http.Request) {
	setHeaders(w, r)
	w.WriteHeader(http.StatusOK)
}

func enableCors(w *http.ResponseWriter) {
	(*w).Header().Set("Access-Control-Allow-Origin", "*")
}

func setHeaders(w http.ResponseWriter, r *http.Request) {
	//log.Println("new request: ", r.URL)
	//diplomkaDb.GetDbStats()
	w.Header().Set("content-type", "application/json")
	//w.Header().Set("content-encoding", "gzip")
	w.Header().Set("Access-Control-Allow-Origin", "*")
	w.Header().Set("Access-Control-Allow-Methods", "POST, GET, OPTIONS, PUT, DELETE")
	w.Header().Set("Access-Control-Allow-Headers", "Accept, Content-Type, Content-Length, Accept-Encoding, X-CSRF-Token, Authorization, User-Device-Type")
}